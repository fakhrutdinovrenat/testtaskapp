using ContractsManagment.Models;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System.Text.Json.Serialization;
using System.Text.Json;
using Azure;

var builder = WebApplication.CreateBuilder(args);

ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

string connection = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(connection));

builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.MapFallbackToFile("index.html"); ;

JsonSerializerOptions options = new()
{
    ReferenceHandler = ReferenceHandler.IgnoreCycles,
    WriteIndented = true
};

app.MapGet("/GetContracts", async (ApplicationContext db) =>
{
    var contractList = await db.Contracts.ToListAsync();
    var stageList = await db.ContractStages.ToListAsync();
    foreach (var contract in contractList)
    {
        var stages = stageList.Where(stage => stage.ContractId == contract.Id).ToList();
        if (stages.Any())
        {
            contract.Stages = stages;
        }
    }
    return Results.Json(contractList, options);
});

app.MapGet("/GetContractStages/{cypher}", async (string cypher, ApplicationContext db) =>
{

    var stageList = await db.ContractStages.ToListAsync();
    var contract = await db.Contracts.FirstOrDefaultAsync(c => c.Cypher == cypher);
    if (contract == null)
    {
        return Results.NotFound(string.Format("�� ������� ����� �������� �� ����� {0}", cypher));
    }
    stageList.Where(cs => cs.ContractId == contract.Id);
    return Results.Json(stageList, options);
});

app.MapPost("/UploadFile", async (HttpContext context, ApplicationContext db) =>
{
    var response = context.Response;
    var request = context.Request;

    response.ContentType = "text/html; charset=utf-8";
    IFormFile? file = request.Form.Files.FirstOrDefault();
    if (file == null || !file.FileName.EndsWith(".xlsx"))
    {
        await response.WriteAsync("�� ������� ��������� ���� ���� ����������� ���� �� ��������������� ����");
        return;
    }
    var uploadPath = $"{Directory.GetCurrentDirectory()}/uploads";
    Directory.CreateDirectory(uploadPath);
    string fullPath = $"{uploadPath}/{file.FileName}";
    var fileLocation = new FileInfo(fullPath);

    using (var fileStream = new FileStream(fullPath, FileMode.Create))
    {
        await file.CopyToAsync(fileStream);
    }

    using (ExcelPackage package = new ExcelPackage(fileLocation))
    {
        #region ��������� ���������
        ExcelWorksheet contractSheet = package.Workbook.Worksheets["����1"];
        int contractRows = contractSheet.Dimension.Rows;

        var contractList = new List<Contract>();
        var dbContracts = await db.Contracts.ToListAsync();
        var dbContractStages = await db.ContractStages.ToListAsync();

        for (int i = 2; i <= contractRows; i++)
        {
            var cypher = contractSheet.Cells[i, 1].Value.ToString();
            if (dbContracts.Any<Contract>(c => c.Cypher == cypher))
                continue;

            contractList.Add(new Contract
            {
                Cypher = cypher,
                Title = contractSheet.Cells[i, 2].Value.ToString(),
                Customer = contractSheet.Cells[i, 3].Value.ToString()
            });
        }

        db.Contracts.AddRange(contractList);
        #endregion

        #region ��������� ������ ���������
        ExcelWorksheet stageSheet = package.Workbook.Worksheets["����2"];
        int stageRows = stageSheet.Dimension.Rows;

        var stageList = new List<ContractStage>();

        for (int i = 2; i <= stageRows; i++)
        {
            var cypher = stageSheet.Cells[i, 1].Value.ToString();
            var name = stageSheet.Cells[i, 2].Value.ToString();

            var contract = db.Contracts.FirstOrDefault(x => x.Cypher == cypher);

            if (contract == null)
                continue;

            // todo �������� �� ������� ������������ ������ � ��������
            var currentContractStages = dbContractStages.Where(stage => stage.ContractId == contract.Id);
            if (currentContractStages.Any(cs => cs.Name == name))
                continue;

            stageList.Add(new ContractStage
            {
                Name = name,
                Start = DateTime.Parse(stageSheet.Cells[i, 3].Value.ToString()),
                End = DateTime.Parse(stageSheet.Cells[i, 4].Value.ToString()),
                ContractId = contract.Id,
                Contract = contract,
            });
        }

        db.ContractStages.AddRange(stageList);
        #endregion

        db.SaveChanges();
    }
    await response.WriteAsync("������ �� ����� ������� ���������.");
});

app.Run();
