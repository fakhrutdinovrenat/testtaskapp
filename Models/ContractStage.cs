﻿namespace ContractsManagment.Models
{
    public class ContractStage
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public int ContractId { get; set; }
        public Contract Contract { get; set; } = null!;
    }
}
