﻿using Microsoft.EntityFrameworkCore;

namespace ContractsManagment.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Contract> Contracts { get; set; } = null!;
        public DbSet<ContractStage> ContractStages { get; set; } = null!;
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContractStage>()
            .HasOne(cs => cs.Contract)
            .WithMany(c => c.Stages)
            .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
