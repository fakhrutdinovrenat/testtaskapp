﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContractsManagment.Models
{
    [Table("contracts")]
    public class Contract
    {
        public int Id { get; set; }
        public string Cypher { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;
        public string Customer { get; set; } = string.Empty;
        public List<ContractStage>? Stages { get; set; }
    }
}
