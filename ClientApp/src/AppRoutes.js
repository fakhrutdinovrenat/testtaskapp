import { Contracts } from "./components/Contracts";
import { Home } from "./components/Home";
import { Import } from "./components/Import";

const AppRoutes = [
  {
    index: true,
    element: <Home />
  },
  {
    path: '/Contracts',
    element: <Contracts />
  },
  {
    path: '/Import',
    element: <Import />
  },
];

export default AppRoutes;
