﻿import React, { Component } from 'react';
import axios from 'axios';

export class Import extends Component {
    static displayName = Import.name;

    constructor(props) {
        super(props);
        this.state = {
            selectedFile: '',
            status: '',
            progress: 0
        }
    }
    selectFileHandler = (event) => {
        let file = event.target.files;
        
        this.setState({
            selectedFile: file[0]
        });
        
    };

    uploadHandler = (event) => {
        const formData = new FormData();
        formData.append('file', this.state.selectedFile);

        axios.post("/UploadFile", formData, {
            onUploadProgress: progressEvent => {
                this.setState({
                    progress: (progressEvent.loaded / progressEvent.total * 100)
                })
            }
        })
            .then((response) => {
                this.setState({ status: `upload success ${response.data}` });
            })
            .catch((error) => {
                this.setState({ status: `upload failed ${error}` });
            })
    }
    render() {
        return (
            <div>
                <h2>Загрузите файл в формате xlsx</h2>
                <div>
                    <input type="file" onChange={this.selectFileHandler} />
                </div>
                <hr />
                <div>
                    <button type="button" onClick={this.uploadHandler}>Upload</button></div>
                <hr />
                <div>{this.state.progress}</div>
                <br />
                <div>{this.state.status}</div>
            </div>
        );
    }
}
