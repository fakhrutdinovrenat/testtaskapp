﻿import React, { Component } from 'react';

export class Contracts extends Component {
    static displayName = Contracts.name;

    constructor(props) {
        super(props);
        this.state = { contracts: [], loading: true };
    }

    componentDidMount() {
        this.fetchContractData();
    }

    static renderTable(contracts) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Шифр</th>
                        <th>Наименование договора</th>
                        <th>Заказчик</th>
                        <th>Количество этапов</th>
                    </tr>
                </thead>
                <tbody>
                    {contracts.map(contract =>
                        <tr key={contract.Id}>
                            <td>{contract.Cypher}</td>
                            <td>{contract.Title}</td>
                            <td>{contract.Customer}</td>
                            <td>{contract.Stages?.length}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Contracts.renderTable(this.state.contracts);

        return (
            <div>
                <h1 id="tabelLabel" >Контракты</h1>
                {contents}
            </div>
        );
    }

    async fetchContractData() {
        const response = await fetch('GetContracts');
        const data = await response.json();
        this.setState({ contracts: data, loading: false });
    }
}
